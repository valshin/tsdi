export type Uuid = string;

export function uuid(): Uuid {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, (char): string => {
    let random = (Math.random() * 16) | 0;
    let value = char === "x" ? random : (random % 4) + 8;
    return value.toString(16);
  });
}
