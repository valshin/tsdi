/* eslint-disable @typescript-eslint/no-unused-vars */

import { bootstrap, Component, Context, destroy, getContext, getContextBean, InjectProperty, restart } from "../api";

describe("integration tests", (): void => {
  describe("if all components have been registered", (): void => {
    @Component
    class DependentOne {
      public name = "dependent one";
    }

    @Component
    class DependentTwo {
      @InjectProperty
      public dependentOne: DependentOne;
      public name = "dependent two";
    }

    @Context
    class ContextHolderOne {
      @InjectProperty
      public dependentTwo: DependentTwo;
      @InjectProperty
      public dependentOne: DependentOne;
    }

    @Context
    class ContextHolderTwo {
      @InjectProperty
      public dependentTwo: DependentTwo;
      @InjectProperty
      public dependentOne: DependentOne;
    }
    describe(`when two contexts are declared`, (): void => {
      beforeEach(
        async (): Promise<void> => {
          await bootstrap();
        },
      );
      afterEach(
        async (): Promise<void> => {
          await destroy();
        },
      );
      it(`context beans are initialized`, (): void => {
        const contextOne = getContextBean(ContextHolderOne);
        const contextTwo = getContextBean(ContextHolderTwo);
        contextOne.dependentTwo.dependentOne.name = "one";
        contextTwo.dependentTwo.dependentOne.name = "two";
        expect(contextOne.dependentTwo.dependentOne.name).toBe("one");
        expect(contextOne.dependentOne.name).toBe("one");
        expect(contextTwo.dependentTwo.dependentOne.name).toBe("two");
        expect(contextTwo.dependentOne.name).toBe("two");
      });
      it(`created beans are different for two contexts and same inside particular context`, (): void => {
        const contextOne = getContext(ContextHolderOne);
        const contextTwo = getContext(ContextHolderTwo);
        contextOne.getBean(DependentTwo).dependentOne.name = "one";
        contextTwo.getBean(DependentTwo).dependentOne.name = "two";
        expect(contextOne.getBean(DependentTwo).dependentOne.name).toBe("one");
        expect(contextOne.getBean(DependentOne).name).toBe("one");
        expect(contextTwo.getBean(DependentTwo).dependentOne.name).toBe("two");
        expect(contextTwo.getBean(DependentOne).name).toBe("two");
      });
      it(`should reset context`, async (): Promise<void> => {
        const contextOne = getContext(ContextHolderOne);
        contextOne.getBean(DependentTwo).dependentOne.name = "one";
        expect(contextOne.getBean(DependentTwo).dependentOne.name).toBe("one");
        expect(contextOne.getBean(DependentOne).name).toBe("one");
        await restart();
        const newContextOne = getContext(ContextHolderOne);
        expect(newContextOne.getBean(DependentTwo).dependentOne.name).toBe("dependent one");
        expect(newContextOne.getBean(DependentOne).name).toBe("dependent one");
      });
    });
  });
  describe("if some component HAS NOT been registered", (): void => {
    it("should throw error", async (): Promise<void> => {
      @Component
      class DependentOne {
        public name = "dependent one";
      }

      class DependentTwo {
        @InjectProperty
        public dependentOne: DependentOne;
        public name = "dependent two";
      }

      @Context
      class ContextHolder {
        @InjectProperty
        public dependentTwo: DependentTwo;
        @InjectProperty
        public dependentOne: DependentOne;
      }
      try {
        await bootstrap();
      } catch (e) {
        expect(e.message).toContain("DependentTwo");
      }
    });
  });
});
