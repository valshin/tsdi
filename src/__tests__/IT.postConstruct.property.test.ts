/* eslint-disable @typescript-eslint/no-unused-vars */

import { bootstrap, Component, Context, destroy, getContextBean, InjectProperty, PostConstruct } from "../api";

function getAsyncValue<T>(value: T, delay = 0): Promise<T> {
  return new Promise(res => setTimeout(() => res(value), delay));
}

describe("should init async property", (): void => {
  describe("when class have any post construct method", (): void => {
    @Component
    class DependentOne {
      public asyncName: string;
      @PostConstruct
      public async anyMethod() {
        this.asyncName = await getAsyncValue("async dependent one", 10);
      }
    }
    @Component
    class DependentTwo {
      public asyncName: string;
      public asyncNameOfDependent: string;
      @InjectProperty
      public dependentOne: DependentOne;
      @PostConstruct
      public async otherMethod() {
        this.asyncName = await getAsyncValue("async dependent two", 1);
        this.asyncNameOfDependent = this.dependentOne.asyncName;
      }
    }
    @Context
    class ContextHolder {
      @InjectProperty
      public dependentOne: DependentOne;
      @InjectProperty
      public dependentTwo: DependentTwo;
    }

    beforeEach(async (): Promise<void> => bootstrap());
    afterEach(async (): Promise<void> => destroy());

    it("should execute after construction", () => {
      const context = getContextBean(ContextHolder);
      expect(context.dependentOne.asyncName).toBe("async dependent one");
    });
    it("should execute after construction in right order", () => {
      const context = getContextBean(ContextHolder);
      expect(context.dependentOne.asyncName).toBe("async dependent one");
      expect(context.dependentTwo.asyncNameOfDependent).toBe("async dependent one");
      expect(context.dependentTwo.asyncName).toBe("async dependent two");
    });
  });
});
