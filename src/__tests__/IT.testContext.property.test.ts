/* eslint-disable @typescript-eslint/no-unused-vars */

import {
  bootstrap,
  Component,
  Context,
  getContext,
  getContextBean,
  InjectProperty,
  TestComponent,
  testComponentFactory,
} from "../api";

describe("integration test", (): void => {
  @Component
  class DependentOne {
    public name = "dependent one";
  }

  @Component
  class DependentTwo {
    @InjectProperty
    public dependentOne: DependentOne;
    public name = "dependent two";
  }

  @Context
  class ContextHolder {
    @InjectProperty
    public dependentTwo: DependentTwo;
    @InjectProperty
    public dependentOne: DependentOne;
  }
  describe("test bean", (): void => {
    describe("should be injected", (): void => {
      @TestComponent(DependentOne)
      class DependentOneMock {
        public name = "mocked dependent one";
      }
      beforeEach(
        async (): Promise<void> => {
          await bootstrap();
        },
      );
      it("injected property should have new value", (): void => {
        expect(getContextBean(ContextHolder).dependentOne.name).toBe("mocked dependent one");
        expect(getContextBean(ContextHolder).dependentTwo.dependentOne.name).toBe("mocked dependent one");
      });
    });
  });
  describe("test bean factory", (): void => {
    describe("can mock bean independently", (): void => {
      it("for test ONE", async (): Promise<void> => {
        testComponentFactory(
          DependentOne,
          (): DependentOne => ({
            name: "factory mocked dependent one test one",
          }),
        );
        await bootstrap();
        expect(getContextBean(ContextHolder).dependentOne.name).toBe("factory mocked dependent one test one");
        expect(getContextBean(ContextHolder).dependentTwo.dependentOne.name).toBe(
          "factory mocked dependent one test one",
        );
      });
      it("for test TWO", async (): Promise<void> => {
        interface DependentOneMock {
          name: string;
          mockedProperty: string;
        }
        testComponentFactory(
          DependentOne,
          (): DependentOne => ({
            name: "factory mocked dependent one test two",
          }),
        );
        await bootstrap();
        expect(getContext(ContextHolder).getTestBean<DependentOneMock>(DependentOne).name).toBe(
          "factory mocked dependent one test two",
        );
        expect(getContextBean(ContextHolder).dependentOne.name).toBe("factory mocked dependent one test two");
        expect(getContextBean(ContextHolder).dependentTwo.dependentOne.name).toBe(
          "factory mocked dependent one test two",
        );
      });
    });
  });
});
