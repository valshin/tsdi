/* eslint-disable @typescript-eslint/no-unused-vars */

import { BeforeDestroy, bootstrap, Component, Context, destroy, getContext, InjectProperty } from "../api";
import { BEAN_NOT_FOUND_ERROR } from "../Context/BeanStorage";

function getAsyncValue<T>(value: T): Promise<T> {
  return new Promise(res => setImmediate(() => res(value)));
}

describe("should destroy", (): void => {
  describe("when class have any before destroy method", (): void => {
    const callOrder: string[] = [];

    @Component
    class DependentOne {
      @BeforeDestroy
      public async anyMethod() {
        callOrder.push(await getAsyncValue("async dependent one"));
      }
    }

    @Component
    class DependentTwo {
      @InjectProperty
      public dependentOne: DependentOne;

      @BeforeDestroy
      public async otherMethod() {
        callOrder.push(await getAsyncValue("async dependent two"));
      }
    }

    @Context
    class ContextHolder {
      @InjectProperty
      public dependentTwo: DependentTwo;
      @InjectProperty
      public dependentOne: DependentOne;
    }

    beforeEach(
      async (): Promise<void> => {
        await bootstrap();
        await destroy();
      },
    );

    it("should execute in right order", () => {
      expect(callOrder).toEqual(["async dependent two", "async dependent one"]);
    });
    it("the context bean store should be empty", () => {
      const context = getContext(ContextHolder);
      expect(() => context.getBean(ContextHolder)).toThrow(BEAN_NOT_FOUND_ERROR);
      expect(() => context.getBean(DependentOne)).toThrow(BEAN_NOT_FOUND_ERROR);
      expect(() => context.getBean(DependentTwo)).toThrow(BEAN_NOT_FOUND_ERROR);
    });
  });
});
