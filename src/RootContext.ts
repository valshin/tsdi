import { BeanRegistry } from "./BeanRegistry/BeanRegistry";
import { AnyConstructor, Constructor } from "./Constructor";
import { TsdiContext } from "./Context/TsdiContext";
import { MetadataUtils } from "./MtadataUtils/MetadataUtils";
import { uuid, Uuid } from "./Utils";

export class RootContext {
  private static contextStore = new Map<Uuid, TsdiContext>();
  private static componentFactory = new BeanRegistry();

  public static registerContext<T extends AnyConstructor>(ctor: T): void {
    const contextId = uuid();
    RootContext.componentFactory.registerCandidate(ctor);
    RootContext.contextStore.set(contextId, new TsdiContext(ctor, contextId, RootContext.componentFactory));
  }

  public static registerComponent<T extends AnyConstructor>(ctor: T): void {
    RootContext.componentFactory.registerCandidate(ctor);
  }

  public static registerComponentFactory<T>(ctor: Constructor<T>, factory: () => T, qualifier?: string): void {
    RootContext.componentFactory.registerComponentFactory(ctor, factory, qualifier);
  }

  public static registerTestComponent<T>(ctor: Constructor<T>): <T extends AnyConstructor>(testCtor: T) => void {
    return function<T extends AnyConstructor>(testCtor: T): void {
      RootContext.componentFactory.registerTestCandidate(ctor, () => new testCtor());
    };
  }

  public static registerTestComponentFactory<T>(
    ctor: Constructor<T>,
    factory: () => Partial<T>,
    qualifier?: string,
  ): void {
    RootContext.componentFactory.registerTestCandidate(ctor, factory, qualifier);
  }

  public static getContext<T>(ctxCtor: Constructor<T>): TsdiContext {
    const contextId = MetadataUtils.getContextId(ctxCtor);
    return RootContext.contextStore.get(contextId);
  }

  public static getContextBean<T>(ctxCtor: Constructor<T>): T {
    const contextId = MetadataUtils.getContextId(ctxCtor);
    return RootContext.contextStore.get(contextId).getBean(ctxCtor);
  }

  public static async bootstrap(): Promise<void> {
    await Promise.all(
      Array.from(RootContext.contextStore.values()).map((context): Promise<void> => context.bootstrap()),
    );
  }

  public static async destroy(): Promise<void> {
    await Promise.all(Array.from(RootContext.contextStore.values()).map((context): Promise<void> => context.destroy()));
  }

  public static async restart(): Promise<void> {
    await RootContext.destroy();
    await RootContext.bootstrap();
  }
}
