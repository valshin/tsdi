import { MetadataUtils } from "./MtadataUtils/MetadataUtils";
import { RootContext } from "./RootContext";

export const InjectProperty = MetadataUtils.markPropertyAsInjectable;
export const InjectParam = MetadataUtils.markInjectableParameter;
export const PostConstruct = MetadataUtils.markPostConstructMethod;
export const BeforeDestroy = MetadataUtils.markBeforeDestroyMethod;
export const Context = RootContext.registerContext;
export const Component = RootContext.registerComponent;
export const componentFactory = RootContext.registerComponentFactory;
export const getContext = RootContext.getContext;
export const getContextBean = RootContext.getContextBean;

export const destroy = RootContext.destroy;
export const bootstrap = RootContext.bootstrap;
export const restart = RootContext.restart;

export const TestComponent = RootContext.registerTestComponent;
export const testComponentFactory = RootContext.registerTestComponentFactory;
