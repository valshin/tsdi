import { AnyConstructor } from "../Constructor";
import { BeanCandidate } from "../BeanCandidate/BeanCandidate";

export class ParameterBeanCandidate extends BeanCandidate {
  public readonly index: number;

  public constructor(ctor: AnyConstructor, index: number, qualifier?: string) {
    super(ctor, qualifier);
    this.index = index;
  }
}
