/* eslint-disable @typescript-eslint/no-explicit-any */
import "reflect-metadata";
import { AnyConstructor } from "../Constructor";
import { Uuid } from "../Utils";
import { ParameterBeanCandidate } from "./ParameterBeanCandidate";
import { PropertyBeanCandidate } from "./PropertyBeanCandidate";
import { BeanCandidateStore } from "../BeanCandidate/BeanCandidateStore";

type PropertyDecorator = (prototype: any, propertyKey: string) => void;
type ParameterDecorator = (target: any, propertyKey: string | symbol, parameterIndex: number) => void;

export class MetadataUtils {
  private static contextIdKey = Symbol("context_id");
  private static injectedPropertiesStoreKey = Symbol("injected_properties");
  private static injectedParametersStoreKey = Symbol("injected_parameters");
  private static postConstructMethodKey = Symbol("post_construct");
  private static beforeDestroyMethodKey = Symbol("before_destroy");

  public static getInjectableProperties(type: AnyConstructor): BeanCandidateStore<PropertyBeanCandidate> {
    return MetadataUtils.getInjectablePropertiesFromPrototype(type.prototype);
  }

  public static getInjectableConstructorParameters(type: AnyConstructor): BeanCandidateStore<ParameterBeanCandidate> {
    return MetadataUtils.getInjectableConstructorParametersFromPrototype(type.prototype);
  }

  public static markPropertyAsInjectable(prototype: any, propertyKey: string): void {
    MetadataUtils.doMarkPropertyAsInjectable(prototype, propertyKey);
  }

  public static markPropertyAsInjectableWithQualifier(qualifier: string): PropertyDecorator {
    return (prototype: any, propertyKey: string): void => {
      MetadataUtils.doMarkPropertyAsInjectable(prototype, propertyKey, qualifier);
    };
  }

  public static markInjectableParameter(target: any, propertyKey: string | symbol, parameterIndex: number): void {
    MetadataUtils.doMarkInjectableParameter(target, propertyKey, parameterIndex);
  }

  public static markPostConstructMethod(prototype: any, propertyKey: string): void {
    Reflect.defineMetadata(MetadataUtils.postConstructMethodKey, propertyKey, prototype);
  }

  public static markBeforeDestroyMethod(prototype: any, propertyKey: string): void {
    Reflect.defineMetadata(MetadataUtils.beforeDestroyMethodKey, propertyKey, prototype);
  }

  public static markInjectableParameterWithQualifier(qualifier: string): ParameterDecorator {
    return (target: any, propertyKey: string | symbol, parameterIndex: number): void => {
      MetadataUtils.doMarkInjectableParameter(target, propertyKey, parameterIndex, qualifier);
    };
  }

  public static getContextId(ctor: AnyConstructor): Uuid {
    return Reflect.getOwnMetadata(MetadataUtils.contextIdKey, ctor.prototype);
  }

  public static defineContextId(target: any, id: Uuid): void {
    Reflect.defineMetadata(MetadataUtils.contextIdKey, id, target);
  }

  public static injectProperty(target: any, propertyKey: string, value: any): void {
    Reflect.defineProperty(target, propertyKey, { value });
  }

  private static doMarkInjectableParameter(
    target: any,
    propertyKey: string | symbol,
    parameterIndex: number,
    qualifier?: string,
  ): void {
    const isConstructor = !propertyKey;
    const paramTypes = Reflect.getMetadata("design:paramtypes", target, propertyKey);
    if (isConstructor) {
      const ctor = target;
      const ctorParams: BeanCandidateStore<ParameterBeanCandidate> = MetadataUtils.getInjectableConstructorParameters(
        ctor,
      );
      paramTypes.forEach((ctor: AnyConstructor, index: number): void => {
        if (index === parameterIndex) {
          ctorParams.addRecord(new ParameterBeanCandidate(ctor, index, qualifier));
        }
      });
    }
  }

  private static doMarkPropertyAsInjectable(prototype: any, propertyKey: string, qualifier?: string): void {
    const ctor = Reflect.getMetadata("design:type", prototype, propertyKey);
    const record = new PropertyBeanCandidate(ctor, propertyKey, qualifier);
    const propertiesToInject: BeanCandidateStore<PropertyBeanCandidate> = MetadataUtils.getInjectablePropertiesFromPrototype(
      prototype,
    );
    propertiesToInject.addRecord(record);
  }

  private static getInjectablePropertiesFromPrototype(prototype: any): BeanCandidateStore<PropertyBeanCandidate> {
    let store = Reflect.getOwnMetadata(MetadataUtils.injectedPropertiesStoreKey, prototype);
    if (!store) {
      store = new BeanCandidateStore<PropertyBeanCandidate>();
      Reflect.defineMetadata(MetadataUtils.injectedPropertiesStoreKey, store, prototype);
    }
    return store;
  }

  private static getInjectableConstructorParametersFromPrototype(
    prototype: any,
  ): BeanCandidateStore<ParameterBeanCandidate> {
    let store = Reflect.getOwnMetadata(MetadataUtils.injectedParametersStoreKey, prototype);
    if (!store) {
      store = new BeanCandidateStore<ParameterBeanCandidate>();
      Reflect.defineMetadata(MetadataUtils.injectedParametersStoreKey, store, prototype);
    }
    return store;
  }

  public static getPostConstructMethodFromPrototype(prototype: any): string {
    return Reflect.getOwnMetadata(MetadataUtils.postConstructMethodKey, prototype);
  }

  public static getBeforeDestroyMethodFromPrototype(prototype: any): string {
    return Reflect.getOwnMetadata(MetadataUtils.beforeDestroyMethodKey, prototype);
  }
}
