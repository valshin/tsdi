import { AnyConstructor } from "../Constructor";
import { BeanCandidate } from "../BeanCandidate/BeanCandidate";

export class PropertyBeanCandidate extends BeanCandidate {
  public readonly name: string;

  public constructor(ctor: AnyConstructor, name: string, qualifier?: string) {
    super(ctor, qualifier);
    this.name = name;
  }
}
