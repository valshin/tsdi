import { DEFAULT_QUALIFIER } from "../../Constants";
import { AnyConstructor } from "../../Constructor";
import { BeanCandidate } from "../../BeanCandidate/BeanCandidate";
import { BeanCandidateStore } from "../../BeanCandidate/BeanCandidateStore";
import { MetadataUtils } from "../MetadataUtils";

function findAndCheck<T extends BeanCandidate, K extends keyof T>(
  store: BeanCandidateStore<T>,
  field: K,
  expected: T[K],
  ctor?: AnyConstructor,
  index?: number,
): void {
  if (!ctor) {
    store.getRecords().forEach((record: T): void => {
      expect(record[field]).toBe(expected);
    });
    return;
  }
  const record = store.getRecords().filter((record: T): boolean => record.ctor === ctor)[index || 0];
  const value = record[field];
  expect(value).toBe(expected);
}
function checkRecordsLength<T extends BeanCandidate>(
  store: BeanCandidateStore<T>,
  length: number,
  ctor?: AnyConstructor,
): void {
  if (!ctor) {
    expect(store.getRecords()).toHaveLength(length);
    return;
  }
  expect(store.getRecords().filter((record: T): boolean => record.ctor === ctor)).toHaveLength(length);
}

describe(`MetadataUtils`, (): void => {
  describe(`Properties`, (): void => {
    it(`without qualifier`, (): void => {
      class DependentOne {}
      class DependentTwo {}
      class TestClass {
        @MetadataUtils.markPropertyAsInjectable
        public propertyOne: DependentOne;
        @MetadataUtils.markPropertyAsInjectable
        public propertyTwo: DependentTwo;
        @MetadataUtils.markPropertyAsInjectable
        public propertyThree: DependentTwo;
      }

      const store = MetadataUtils.getInjectableProperties(TestClass);

      checkRecordsLength(store, 3);
      checkRecordsLength(store, 1, DependentOne);
      checkRecordsLength(store, 2, DependentTwo);
      findAndCheck(store, "name", "propertyOne", DependentOne);
      findAndCheck(store, "name", "propertyTwo", DependentTwo, 0);
      findAndCheck(store, "name", "propertyThree", DependentTwo, 1);
      findAndCheck(store, "qualifier", DEFAULT_QUALIFIER);
      findAndCheck(store, "qualifier", DEFAULT_QUALIFIER);
      findAndCheck(store, "qualifier", DEFAULT_QUALIFIER);
    });
    it(`with qualifier`, (): void => {
      class DependentOne {}
      class DependentTwo {}
      class TestClass {
        @MetadataUtils.markPropertyAsInjectableWithQualifier("qualifierOne")
        public propertyOne: DependentOne;
        @MetadataUtils.markPropertyAsInjectableWithQualifier("qualifierTwo")
        public propertyTwo: DependentTwo;
      }

      const store = MetadataUtils.getInjectableProperties(TestClass);
      checkRecordsLength(store, 2);
      findAndCheck(store, "name", "propertyOne", DependentOne);
      findAndCheck(store, "qualifier", "qualifierOne", DependentOne);
      findAndCheck(store, "name", "propertyTwo", DependentTwo);
      findAndCheck(store, "qualifier", "qualifierTwo", DependentTwo);
    });
  });
  describe(`Parameters`, (): void => {
    describe(`constructor parameters`, (): void => {
      it(`without qualifier`, (): void => {
        class DependentOne {}
        class DependentTwo {}
        class TestClass {
          public constructor(
            @MetadataUtils.markInjectableParameter parameterOne: DependentOne,
            @MetadataUtils.markInjectableParameter parameterTwo: DependentTwo,
          ) {
            this.propertyOne = parameterOne;
            this.propertyTwo = parameterTwo;
          }
          public propertyOne: DependentOne;
          public propertyTwo: DependentTwo;
        }

        const store = MetadataUtils.getInjectableConstructorParameters(TestClass);
        checkRecordsLength(store, 2);
        findAndCheck(store, "index", 0, DependentOne);
        findAndCheck(store, "qualifier", DEFAULT_QUALIFIER, DependentOne);
        findAndCheck(store, "index", 1, DependentTwo);
        findAndCheck(store, "qualifier", DEFAULT_QUALIFIER, DependentTwo);
      });
      it(`with qualifier`, (): void => {
        class DependentOne {}
        class DependentTwo {}
        class TestClass {
          public constructor(
            @MetadataUtils.markInjectableParameterWithQualifier("qualifierOne") parameterOne: DependentOne,
            @MetadataUtils.markInjectableParameterWithQualifier("qualifierTwo") parameterTwo: DependentTwo,
          ) {
            this.propertyOne = parameterOne;
            this.propertyTwo = parameterTwo;
          }
          public propertyOne: DependentOne;
          public propertyTwo: DependentTwo;
        }

        const store = MetadataUtils.getInjectableConstructorParameters(TestClass);
        checkRecordsLength(store, 2);
        findAndCheck(store, "index", 0, DependentOne);
        findAndCheck(store, "qualifier", "qualifierOne", DependentOne);
        findAndCheck(store, "index", 1, DependentTwo);
        findAndCheck(store, "qualifier", "qualifierTwo", DependentTwo);
      });
    });
  });
});
