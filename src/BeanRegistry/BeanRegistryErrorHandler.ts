import { DEFAULT_QUALIFIER } from "../Constants";
import { ComponentFactorySearchData } from "./BeanRegistry";

export class ComponentFactoryError extends Error {
  public constructor(message: string) {
    super(message);
    this.name = "BeanRegistryError";
  }
}

export class BeanRegistryErrorHandler {
  public static throwError<T>({
    typeDefinitionsCount,
    qualifierDefinitionsCount,
    primaryDefinitionsCount,
    ctor,
    qualifier,
  }: ComponentFactorySearchData<T>): void {
    const isDefaultQualifier = qualifier === DEFAULT_QUALIFIER;
    if (typeDefinitionsCount === 0) {
      throw new ComponentFactoryError(
        `Cannot find component with name '${ctor.name}' and qualifier '${qualifier}'. It has to be registered with '@Component' decorator or manually`,
      );
    }
    if (qualifierDefinitionsCount === 0 && !isDefaultQualifier) {
      throw new ComponentFactoryError(`Cannot find component with qualifier "${qualifier}"`);
    }
    let message = `Several candidates`;
    if (!isDefaultQualifier) {
      message = `${message} with "${qualifier}" qualifier`;
    }
    message = `${message} were found, possible solutions:\n- try to use unique qualifier\n`;
    if (primaryDefinitionsCount === 0) {
      message = `${message}- try to use "@Primary" decorator\n`;
    }
    if (primaryDefinitionsCount > 1) {
      message = `${message}- use "@Primary" decorator only once for given type-qualifier combination\n`;
    }
    throw new ComponentFactoryError(message.trim());
  }
}
