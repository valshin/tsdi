import { BeanCandidateStore } from "../BeanCandidate/BeanCandidateStore";
import { DEFAULT_QUALIFIER } from "../Constants";
import { Constructor } from "../Constructor";
import { BeanRegistryCandidate } from "./BeanRegistryCandidate";
import { BeanRegistryErrorHandler } from "./BeanRegistryErrorHandler";

export interface ComponentFactorySearchData<T> {
  candidate: BeanRegistryCandidate<T>;
  typeDefinitionsCount: number;
  qualifierDefinitionsCount: number;
  primaryDefinitionsCount: number;
  ctor: Constructor<T>;
  qualifier: string;
}

export class BeanRegistry {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private readonly beanCandidateStore = new BeanCandidateStore<BeanRegistryCandidate<any>>();

  public registerComponentFactory<T>(ctor: Constructor<T>, factory: () => T, qualifier = DEFAULT_QUALIFIER): void {
    this.beanCandidateStore.addRecord(new BeanRegistryCandidate(ctor, qualifier, false, factory));
  }

  public registerCandidate<T>(ctor: Constructor<T>, qualifier = DEFAULT_QUALIFIER): void {
    this.beanCandidateStore.addRecord(new BeanRegistryCandidate(ctor, qualifier));
  }

  public registerTestCandidate<T>(
    ctor: Constructor<T>,
    factory: () => Partial<T>,
    qualifier = DEFAULT_QUALIFIER,
  ): void {
    const search = this.searchComponent(ctor, qualifier);
    if (!search.candidate) {
      BeanRegistryErrorHandler.throwError(search);
    }
    this.beanCandidateStore.replaceRecord(search.candidate, new BeanRegistryCandidate(ctor, qualifier, true, factory));
  }

  public registerPrimaryCandidate<T>(ctor: Constructor<T>, qualifier = DEFAULT_QUALIFIER): void {
    this.beanCandidateStore.addRecord(new BeanRegistryCandidate(ctor, qualifier, true));
  }

  private searchComponent<T>(ctor: Constructor<T>, qualifier = DEFAULT_QUALIFIER): ComponentFactorySearchData<T> {
    let candidate: BeanRegistryCandidate<T> = null;
    const isDefaultQualifier = qualifier === DEFAULT_QUALIFIER;
    const typeDefinitions = this.beanCandidateStore
      .getRecords()
      .filter((record): boolean => record.ctor === ctor || record.ctor.prototype instanceof ctor);
    if (typeDefinitions.length === 1 && isDefaultQualifier) {
      candidate = typeDefinitions[0];
    }
    const qualifierDefinitions = typeDefinitions.filter((record): boolean => record.qualifier === qualifier);
    if (qualifierDefinitions.length === 1) {
      candidate = qualifierDefinitions[0];
    }
    const primaryDefinitions = qualifierDefinitions.filter((record): boolean => record.isPrimary);
    if (primaryDefinitions.length === 1) {
      candidate = primaryDefinitions[0];
    }
    return {
      candidate,
      typeDefinitionsCount: typeDefinitions.length,
      qualifierDefinitionsCount: qualifierDefinitions.length,
      primaryDefinitionsCount: primaryDefinitions.length,
      ctor,
      qualifier,
    };
  }

  private static getComponentInstance<T>(candidate: BeanRegistryCandidate<T>): T {
    return candidate.factory ? candidate.factory() : new candidate.ctor();
  }

  public getComponent<T>(ctor: Constructor<T>, qualifier = DEFAULT_QUALIFIER): T {
    const search = this.searchComponent(ctor, qualifier);
    if (search.candidate) {
      return BeanRegistry.getComponentInstance(search.candidate);
    }
    BeanRegistryErrorHandler.throwError(search);
  }
}
