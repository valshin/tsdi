import { AnyConstructor } from "../Constructor";
import { BeanCandidate } from "../BeanCandidate/BeanCandidate";

export class BeanRegistryCandidate<T> extends BeanCandidate {
  public readonly isPrimary: boolean;
  public readonly factory: () => T;

  public constructor(ctor: AnyConstructor, qualifier: string, isPrimary = false, factory?: () => T) {
    super(ctor, qualifier);
    this.isPrimary = isPrimary;
    this.factory = factory;
  }
}
