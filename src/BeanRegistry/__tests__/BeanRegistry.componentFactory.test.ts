import { BeanRegistry } from "../BeanRegistry";

describe(`on registerComponent call`, (): void => {
  let factory: BeanRegistry;
  beforeEach((): void => {
    factory = new BeanRegistry();
  });
  describe(`without qualifier`, (): void => {
    it(`should return component if it's factory method has been registered`, (): void => {
      class TestComponent {}

      factory.registerComponentFactory(TestComponent, () => new TestComponent());
      expect(factory.getComponent(TestComponent)).toBeInstanceOf(TestComponent);
    });
    it(`should return component if it's factory method has been registered`, (): void => {
      class TestComponent {}
      class NestedTestComponent extends TestComponent {}
      factory.registerComponentFactory(TestComponent, () => new NestedTestComponent());
      expect(factory.getComponent(TestComponent)).toBeInstanceOf(NestedTestComponent);
    });
  });
});
