import { BeanRegistry } from "../BeanRegistry";

describe(`on registerComponent call`, (): void => {
  let factory: BeanRegistry;
  beforeEach((): void => {
    factory = new BeanRegistry();
  });
  describe(`without qualifier`, (): void => {
    it(`should throw an error if component hasn't been registered`, (): void => {
      class TestComponent {}

      expect((): TestComponent => factory.getComponent(TestComponent)).toThrowError("Cannot find component");
    });
    it(`should return component if it has been registered`, (): void => {
      class TestComponent {}

      factory.registerCandidate(TestComponent);
      expect(factory.getComponent(TestComponent)).toBeInstanceOf(TestComponent);
    });
    it(`can request a component by it's superclass`, (): void => {
      class TestComponent {}

      class NestedTestComponent extends TestComponent {}

      factory.registerCandidate(NestedTestComponent);
      expect(factory.getComponent(TestComponent)).toBeInstanceOf(NestedTestComponent);
    });
    it(`can register multiple components`, (): void => {
      class TestComponentOne {}

      class TestComponentTwo {}

      factory.registerCandidate(TestComponentOne);
      factory.registerCandidate(TestComponentTwo);
      expect(factory.getComponent(TestComponentOne)).toBeInstanceOf(TestComponentOne);
      expect(factory.getComponent(TestComponentTwo)).toBeInstanceOf(TestComponentTwo);
    });
    it(`should throw an error if few implementations were registered`, (): void => {
      class TestComponent {}

      class NestedTestComponentOne extends TestComponent {}

      class NestedTestComponentTwo extends TestComponent {}

      factory.registerCandidate(NestedTestComponentOne);
      factory.registerCandidate(NestedTestComponentTwo);
      expect((): TestComponent => factory.getComponent(TestComponent)).toThrowError(
        `Several candidates were found, possible solutions:
- try to use unique qualifier
- try to use "@Primary" decorator`,
      );
    });
  });
  describe(`with qualifier`, (): void => {
    it(`should throw an error if component with given qualifier hasn't been registered`, (): void => {
      class TestComponent {}

      const qualifier = "test qualifier";
      expect((): TestComponent => factory.getComponent(TestComponent, qualifier)).toThrowError("Cannot find component");
    });
    it(`should throw an error if component has been registered with different qualifier`, (): void => {
      class TestComponent {}

      const qualifierOne = "test qualifier one";
      const qualifierTwo = "test qualifier two";
      factory.registerCandidate(TestComponent, qualifierOne);
      expect((): TestComponent => factory.getComponent(TestComponent, qualifierTwo)).toThrowError(
        "Cannot find component",
      );
    });
    it(`can request a component without qualifier and result should be a component even it has been registered with qualifier`, (): void => {
      class TestComponent {}

      const qualifier = "test qualifier";
      factory.registerCandidate(TestComponent, qualifier);
      expect(factory.getComponent(TestComponent)).toBeInstanceOf(TestComponent);
    });
    it(`can request a component without qualifier by it's superclass and result should be a component even it has been registered with qualifier`, (): void => {
      class TestComponent {}

      class NestedTestComponent extends TestComponent {}

      const qualifier = "test qualifier";
      factory.registerCandidate(NestedTestComponent, qualifier);
      expect(factory.getComponent(TestComponent)).toBeInstanceOf(TestComponent);
    });
    it(`should throw an error if few components were registered with qualifier but requested without`, (): void => {
      class TestComponent {}

      class NestedTestComponent extends TestComponent {}

      const qualifierOne = "test qualifier one";
      const qualifierTwo = "test qualifier two";
      factory.registerCandidate(NestedTestComponent, qualifierOne);
      factory.registerCandidate(NestedTestComponent, qualifierTwo);
      expect((): TestComponent => factory.getComponent(TestComponent)).toThrowError(
        `Several candidates were found, possible solutions:
- try to use unique qualifier
- try to use "@Primary" decorator`,
      );
    });
    it(`can request a component with qualifier if it has been registered`, (): void => {
      class TestComponent {}

      const qualifier = "test qualifier";
      factory.registerCandidate(TestComponent, qualifier);
      expect(factory.getComponent(TestComponent, qualifier)).toBeInstanceOf(TestComponent);
    });
    it(`can request a component by it's superclass and qualifier `, (): void => {
      class TestComponent {}

      class NestedTestComponentOne extends TestComponent {}

      class NestedTestComponentTwo extends TestComponent {}

      const qualifierOne = "test qualifier one";
      const qualifierTwo = "test qualifier two";
      factory.registerCandidate(NestedTestComponentOne, qualifierOne);
      factory.registerCandidate(NestedTestComponentTwo, qualifierTwo);
      expect(factory.getComponent(TestComponent, qualifierOne)).toBeInstanceOf(NestedTestComponentOne);
      expect(factory.getComponent(TestComponent, qualifierTwo)).toBeInstanceOf(NestedTestComponentTwo);
    });
    it(`should throw an error if few implementations for given qualifier were registered`, (): void => {
      class TestComponent {}

      class NestedTestComponentOne extends TestComponent {}

      class NestedTestComponentTwo extends TestComponent {}

      const qualifier = "test qualifier";
      factory.registerCandidate(NestedTestComponentOne, qualifier);
      factory.registerCandidate(NestedTestComponentTwo, qualifier);
      expect((): TestComponent => factory.getComponent(TestComponent, qualifier)).toThrowError(
        `Several candidates with "test qualifier" qualifier were found, possible solutions:
- try to use unique qualifier
- try to use "@Primary" decorator`,
      );
    });
  });
  describe(`with primary`, (): void => {
    it(`can register a primary component`, (): void => {
      class TestComponent {}

      factory.registerPrimaryCandidate(TestComponent);
      expect(factory.getComponent(TestComponent)).toBeInstanceOf(TestComponent);
    });
    it(`can register few components and primary has to be returned`, (): void => {
      class TestComponent {}

      class NestedTestComponentOne extends TestComponent {}

      class NestedTestComponentTwo extends TestComponent {}

      factory.registerCandidate(NestedTestComponentOne);
      factory.registerPrimaryCandidate(NestedTestComponentTwo);
      expect(factory.getComponent(TestComponent)).toBeInstanceOf(NestedTestComponentTwo);
    });
    it(`can register few components with same qualifier and primary has to be returned`, (): void => {
      class TestComponent {}

      const qualifier = "test qualifier";

      class NestedTestComponentOne extends TestComponent {}

      class NestedTestComponentTwo extends TestComponent {}

      factory.registerCandidate(NestedTestComponentOne, qualifier);
      factory.registerPrimaryCandidate(NestedTestComponentTwo, qualifier);
      expect(factory.getComponent(TestComponent, qualifier)).toBeInstanceOf(NestedTestComponentTwo);
    });
    it(`should throw an error if few implementations have primary flag`, (): void => {
      class TestComponent {}

      class NestedTestComponentOne extends TestComponent {}

      class NestedTestComponentTwo extends TestComponent {}

      factory.registerPrimaryCandidate(NestedTestComponentOne);
      factory.registerPrimaryCandidate(NestedTestComponentTwo);
      expect((): TestComponent => factory.getComponent(TestComponent)).toThrowError(
        `Several candidates were found, possible solutions:
- try to use unique qualifier
- use "@Primary" decorator only once for given type-qualifier combination`,
      );
    });
    it(`should throw an error if few implementations with the same qualifier have primary flag`, (): void => {
      class TestComponent {}

      class NestedTestComponentOne extends TestComponent {}

      class NestedTestComponentTwo extends TestComponent {}

      const qualifier = "test qualifier";
      factory.registerPrimaryCandidate(NestedTestComponentOne, qualifier);
      factory.registerPrimaryCandidate(NestedTestComponentTwo, qualifier);
      expect((): TestComponent => factory.getComponent(TestComponent, qualifier)).toThrowError(
        `Several candidates with "test qualifier" qualifier were found, possible solutions:
- try to use unique qualifier
- use "@Primary" decorator only once for given type-qualifier combination`,
      );
    });
  });
});
