/* eslint-disable @typescript-eslint/no-explicit-any */

import { AnyConstructor, Constructor } from "../Constructor";

export const BEAN_NOT_FOUND_ERROR = "Bean not found";
export const BEAN_ALREADY_EXISTS = "Bean already exists";

class TypedBeansContainer<T> {
  private readonly beans: Map<string, T> = new Map();
  private readonly Ctor: Constructor<T>;

  public constructor(Ctor: Constructor<T>) {
    this.Ctor = Ctor;
  }

  public addBean(bean: T, qualifier: string): void {
    if (this.beans.has(qualifier)) {
      throw new Error(BEAN_ALREADY_EXISTS);
    }
    this.beans.set(qualifier, bean);
  }

  public replaceBean(bean: T, qualifier: string): void {
    this.beans.set(qualifier, bean);
  }

  public isBeanExist(name: string): boolean {
    return this.beans.has(name);
  }

  public getBean(qualifier: string): T {
    if (!this.beans.has(qualifier)) {
      throw new Error(BEAN_NOT_FOUND_ERROR);
    }
    return this.beans.get(qualifier);
  }

  public removeBean(qualifier: string): void {
    if (!this.beans.has(qualifier)) {
      throw new Error(BEAN_NOT_FOUND_ERROR);
    }
    this.beans.delete(qualifier);
  }

  public doesContainType(Ctor: AnyConstructor): boolean {
    return this.Ctor === Ctor;
  }
}

export class BeanStorage {
  private typedBeansContainers: TypedBeansContainer<any>[] = [];

  public addRecord<T>(bean: T, Ctor: Constructor<T>, qualifier: string): void {
    let typedBeansContainer = this.safelyGetTypedBeansContainer(Ctor);
    typedBeansContainer.addBean(bean, qualifier);
  }

  public isBeanExist<T>(Ctor: Constructor<T>, qualifier: string): boolean {
    let typedBeansContainer = this.getTypedBeansContainer(Ctor);
    return !!typedBeansContainer && typedBeansContainer.isBeanExist(qualifier);
  }

  public getRecord<T>(Ctor: Constructor<T>, qualifier: string): T {
    let typedBeansContainer = this.getTypedBeansContainer(Ctor);
    if (!typedBeansContainer) {
      throw new Error(BEAN_NOT_FOUND_ERROR);
    }
    return typedBeansContainer.getBean(qualifier);
  }

  public removeRecord<T>(Ctor: Constructor<T>, qualifier: string): void {
    let typedBeansContainer = this.getTypedBeansContainer(Ctor);
    if (!typedBeansContainer) {
      throw new Error(BEAN_NOT_FOUND_ERROR);
    }
    typedBeansContainer.removeBean(qualifier);
  }

  public clear(): void {
    this.typedBeansContainers = [];
  }

  private safelyGetTypedBeansContainer<T>(Ctor: Constructor<T>): TypedBeansContainer<T> {
    let typedBeansContainer = this.getTypedBeansContainer(Ctor);
    if (!typedBeansContainer) {
      typedBeansContainer = new TypedBeansContainer<T>(Ctor);
      this.typedBeansContainers.push(typedBeansContainer);
    }
    return typedBeansContainer;
  }

  private getTypedBeansContainer<T>(Ctor: Constructor<T>): TypedBeansContainer<T> {
    return this.typedBeansContainers.find((record): boolean => record.doesContainType(Ctor));
  }
}
