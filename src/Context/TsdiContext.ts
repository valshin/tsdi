/* eslint-disable @typescript-eslint/no-explicit-any */

import { BeanRegistry } from "../BeanRegistry/BeanRegistry";
import { DEFAULT_QUALIFIER } from "../Constants";
import { AnyConstructor, Constructor } from "../Constructor";
import { BeanCandidate } from "../BeanCandidate/BeanCandidate";
import { PropertyBeanCandidate } from "../MtadataUtils/PropertyBeanCandidate";
import { MetadataUtils } from "../MtadataUtils/MetadataUtils";
import { uuid, Uuid } from "../Utils";
import { BeanStorage } from "./BeanStorage";
import { BeanCandidateStore } from "../BeanCandidate/BeanCandidateStore";

export class TsdiContext {
  private readonly id: Uuid;
  private readonly store = new BeanStorage();
  private readonly contextHolderCtor: AnyConstructor;
  private readonly componentFactory: BeanRegistry;
  private beanCandidate: BeanCandidate = null;

  public constructor(holder: AnyConstructor, id: Uuid = uuid(), componentFactory: BeanRegistry) {
    this.id = id;
    this.contextHolderCtor = holder;
    this.componentFactory = componentFactory;
    MetadataUtils.defineContextId(this.contextHolderCtor.prototype, this.id);
  }

  public async bootstrap(): Promise<void> {
    this.store.clear();
    await this.bootstrapContext();
  }

  public async destroy(): Promise<void> {
    await this.destroyContext();
    this.store.clear();
  }

  public getBean<T>(Ctor: Constructor<T>, qualifier = DEFAULT_QUALIFIER): T {
    return this.store.getRecord(Ctor, qualifier);
  }

  public getTestBean<T>(Ctor: AnyConstructor, qualifier = DEFAULT_QUALIFIER): T {
    return this.store.getRecord(Ctor, qualifier) as T;
  }

  private async bootstrapContext(): Promise<void> {
    this.beanCandidate = new BeanCandidate(this.contextHolderCtor);
    await this.initDependencies(this.beanCandidate);
  }

  private async destroyContext(): Promise<void> {
    await this.destroyDependencies(this.beanCandidate);
    this.beanCandidate = null;
  }

  private async destroyDependencies(candidate: BeanCandidate): Promise<void> {
    if (this.store.isBeanExist(candidate.ctor, candidate.qualifier)) {
      await this.callBeforeDestroy(candidate);
      this.nullifyInjectedProperties(candidate);
      this.unregisterContextBean(candidate);
    }
    const injectedProperties = MetadataUtils.getInjectableProperties(candidate.ctor).getRecords();
    for (let index = 0; index < injectedProperties.length; index++) {
      await this.destroyDependencies(injectedProperties[index]);
    }
  }

  private async callBeforeDestroy(candidate: BeanCandidate): Promise<void> {
    const beforeDestroyMethodName = MetadataUtils.getBeforeDestroyMethodFromPrototype(candidate.ctor.prototype);
    const bean = this.store.getRecord(candidate.ctor, candidate.qualifier);
    if (bean[beforeDestroyMethodName]) {
      await bean[beforeDestroyMethodName]();
    }
  }

  private nullifyInjectedProperties(candidate: BeanCandidate): void {
    const store: BeanCandidateStore<PropertyBeanCandidate> = MetadataUtils.getInjectableProperties(candidate.ctor);
    const instance = this.store.getRecord(candidate.ctor, candidate.qualifier);
    store.getRecords().forEach((injectableBeanCandidate: PropertyBeanCandidate): void => {
      const name = injectableBeanCandidate.name;
      MetadataUtils.injectProperty(instance, name, null);
    });
  }

  private unregisterContextBean(candidate: BeanCandidate): void {
    this.store.removeRecord(candidate.ctor, candidate.qualifier);
  }

  private async initDependencies(candidate: BeanCandidate): Promise<void> {
    const injectedProperties = MetadataUtils.getInjectableProperties(candidate.ctor).getRecords();
    for (let index = 0; index < injectedProperties.length; index++) {
      await this.initDependencies(injectedProperties[index]);
    }
    if (!this.store.isBeanExist(candidate.ctor, candidate.qualifier)) {
      this.registerContextBean(candidate);
      this.injectRequiredProperties(candidate);
      await this.callPostConstructs(candidate);
    }
  }

  private registerContextBean(candidate: BeanCandidate): void {
    const bean = this.componentFactory.getComponent(candidate.ctor, candidate.qualifier);
    MetadataUtils.defineContextId(bean, this.id);
    this.store.addRecord(bean, candidate.ctor, candidate.qualifier);
  }

  private injectRequiredProperties(candidate: BeanCandidate): void {
    const store: BeanCandidateStore<PropertyBeanCandidate> = MetadataUtils.getInjectableProperties(candidate.ctor);
    const instance = this.store.getRecord(candidate.ctor, candidate.qualifier);
    store.getRecords().forEach((injectableBeanCandidate: PropertyBeanCandidate): void => {
      const dependentCtor = injectableBeanCandidate.ctor;
      const name = injectableBeanCandidate.name;
      const qualifier = injectableBeanCandidate.qualifier;
      MetadataUtils.injectProperty(instance, name, this.store.getRecord(dependentCtor, qualifier));
    });
  }

  private async callPostConstructs(candidate: BeanCandidate): Promise<void> {
    const postConstructorMethodName = MetadataUtils.getPostConstructMethodFromPrototype(candidate.ctor.prototype);
    const bean = this.store.getRecord(candidate.ctor, candidate.qualifier);
    if (bean[postConstructorMethodName]) {
      await bean[postConstructorMethodName]();
    }
  }
}
