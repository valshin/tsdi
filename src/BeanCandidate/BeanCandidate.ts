import { DEFAULT_QUALIFIER } from "../Constants";
import { AnyConstructor } from "../Constructor";

export class BeanCandidate {
  public readonly ctor: AnyConstructor;
  public readonly qualifier: string;

  public constructor(ctor: AnyConstructor, qualifier = DEFAULT_QUALIFIER) {
    this.ctor = ctor;
    this.qualifier = qualifier;
  }
}
