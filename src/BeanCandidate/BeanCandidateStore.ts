import { BeanCandidate } from "./BeanCandidate";

export class BeanCandidateStore<T extends BeanCandidate> {
  private readonly store = new Set<T>();
  public getRecords(): T[] {
    return Array.from(this.store.values());
  }
  public addRecord(property: T): void {
    this.store.add(property);
  }
  public replaceRecord(oldProperty: T, newProperty: T): void {
    this.store.delete(oldProperty);
    this.store.add(newProperty);
  }
}
