/* eslint-disable @typescript-eslint/no-explicit-any */
export type Constructor<T> = new (...args: any[]) => T;
export type AnyConstructor = Constructor<any>;
